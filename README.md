# Materi Kuliah Dasar-Dasar Pemograman (INF 2007) #

## Penilaian ##
* Formatif (Absensi + Tugas + Quiz) = 30%
* UTS = 30% 
* UAS = 40%

## Gitlab Project ##
* Buat project dengan nama `NIM_inf2007`
* Struktur folder
  ```
  Laporan
  Praktek
   --> src
  Project
   --> src
  Presentasi
  Tugas
  ```
  

## Daftar Mahasiswa ##
* [11190910000002](https://gitlab.com/mahizahrani/11190910000002_inf2007)
* [11190910000003](https://gitlab.com/Psptasr/11190910000003_inf2007)
* [11190910000004](https://gitlab.com/darkanst/11190910000004_inf2007)
* [11190910000005](https://gitlab.com/zein_lab/11190910000005_inf2007)
* [11190910000006](https://gitlab.com/raffyandar/11190910000006_inf2007)
* [11190910000007](https://gitlab.com/Unay/11190910000007_inf2007)
* [11190910000008](https://gitlab.com/aryadhika11/11190910000008_inf2007)
* [11190910000009](https://gitlab.com/mutmainnahmth1/11190910000009_inf2007)
* [11190910000010](https://gitlab.com/Zakipu1210/11190910000010_inf2007)
* [11190910000011](https://gitlab.com/wahyurmd0512/11190910000011_inf2007)
* [11190910000012](https://gitlab.com/IvaAlfiyanti/11190910000012_inf2007)
* [11190910000013](https://gitlab.com/msigit26/11190910000013_inf2007)
* [11190910000014](https://gitlab.com/aghnisyifaahmari/11190910000014_inf2007)
* [11190910000015](https://gitlab.com/DifaRisalahAkbar/11190910000015_inf2007)
* [11190910000016](https://gitlab.com/uswatunhasanah.tkn/11190910000016_inf2007)
* [11190910000017](https://gitlab.com/Fachridan.tm/11190910000017_inf2007)
* [11190910000018](https://gitlab.com/Royan354/11190910000018_inf2007)
* [11190910000019](https://gitlab.com/hilmibill01/11190910000019_inf2007)
* [11190910000020](https://gitlab.com/silmi.akamal/11190910000020-inf2007)
* [11190910000021](https://gitlab.com/Revimaulana/11190910000021_inf2007)
* [11190910000022](https://gitlab.com/Fitroh_amri/11190910000022_inf2007)
* [11190910000023](https://gitlab.com/Rahmalia/11190910000023_inf2007)
